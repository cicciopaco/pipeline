import sys
import os
import bpy

print("INITIALIZING BLENDER")

# Add python path
sys.path.append(r"D:\Blender\scripts")
sys.path.append(r"D:\pipeline\common")

# clean up the scene
bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete() 
# NOTES: we should investigate into delete the dataBlock(?!)


# clear console
#os.system("cls")
